#!/usr/bin/env python3

import sys
import sys
from akapy.helpers import CredentialParser
from akapy.aka import Api
import logging


def main():
    if len(sys.argv) != 3:
        sys.stderr.write("Usage: %s NAME IP\n" % sys.argv[0])
        sys.exit(3)
    else:
        target_name = sys.argv[1]
        target_ip = sys.argv[2]

    logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.DEBUG)

    cred_parser = CredentialParser("https://aka.oit.duke.edu")
    cred = cred_parser.import_credentials()
    AKA = Api("https://aka.oit.duke.edu", cred)

    existing_records = AKA.search_dns(target_name)
    if len(existing_records) > 0:
        print("Deleting existing records")
        AKA.delete_dns(target_name)

    print(AKA.register_a(target_name, target_ip))

    return 0


if __name__ == "__main__":
    sys.exit(main())
