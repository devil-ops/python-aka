#!/usr/bin/env python3

import sys
from akapy.helpers import CredentialParser
from akapy.aka import Api


def main():

    if len(sys.argv) != 2:
        sys.stderr.write("Usage: %s CIDR\n" % sys.argv[0])
        return 1

    cred_parser = CredentialParser("https://aka.oit.duke.edu")
    cred = cred_parser.import_credentials()
    AKA = Api("https://aka.oit.duke.edu", cred)

    print(AKA.get_network(sys.argv[1]))
    return 0


if __name__ == "__main__":
    sys.exit(main())
