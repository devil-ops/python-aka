#!/usr/bin/env python3

import sys
from akapy.helpers import CredentialParser, dns_lookup
from akapy.aka import Api
import argparse


def parse_args():

    description = """
Create a partner A record on the same network as the target.

For example:
./create_partner_dns -s '-foo' example.oit.duke.edu

Would create a new A record on the same network called:

example-foo.oit.duke.edu

    """

    parser = argparse.ArgumentParser(
        description=description)
    parser.add_argument('name', type=str, help='Base name for the partner')
    parser.add_argument('-s', '--suffix', type=str,
                        help='Suffix for partner name')

    return parser.parse_args()


def make_partner_name(name, suffix):

    (hostname, domain) = name.split(".", 1)
    return("%s-%s.%s" % (hostname, suffix, domain))


def main():

    cred_parser = CredentialParser("https://aka.oit.duke.edu")
    cred = cred_parser.import_credentials()

    AKA = Api("https://aka.oit.duke.edu", cred)

    args = parse_args()

    # Make sure the name is valid
    ip = dns_lookup(args.name)
    if not ip:
        sys.stderr.write("Could not look up %s, quitting\n" % args.name)
        return 3

    # Make sure the partner name isn't already taken
    partner_name = make_partner_name(args.name, args.suffix)
    if dns_lookup(partner_name):
        sys.stderr.write("Partner name: %s is already taken, quitting\n" %
                         partner_name)
        return 4

    # Get IP and Subnet
    ip_obj = AKA.get_ip(ip)
    target_subnet = ip_obj['subnet']

    print("Need to register %s on %s" % (partner_name, target_subnet))
    register = AKA.register_next_available(target_subnet, partner_name)
    print(register)

    return 0


if __name__ == "__main__":
    sys.exit(main())
