#!/bin/bash

curl -X 'POST' -H 'Accept: */*' -H 'Accept-Encoding: gzip, deflate' -H 'Authorization: Basic ZHJld3M6Mzg0ZGNiNDgtODlkYy00ZDk5LWFmZjQtMDI2N2U1NTA3Yzdk' -H 'Connection: keep-alive' -H 'Content-Length: 0' -H 'User-Agent: python-requests/2.21.0' 'https://aka.oit.duke.edu/api/v1/dns?name=drew-delete-name-test-01.oit.duke.edu&type=A&ttl=3600&address=104.20.16.242'
