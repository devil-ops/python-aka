#!/usr/bin/env python3

import sys
import os
import requests
import configparser
import logging
logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.DEBUG)


def main():
    if len(sys.argv) != 3:
        sys.stderr.write("Usage: %s NAME IP\n" % sys.argv[0])
        sys.exit(3)
    else:
        target_name = sys.argv[1]
        target_ip = sys.argv[2]

    config = configparser.ConfigParser()

    config.read(os.path.expanduser("~/.akarc"))

    auth = (config['https://aka.oit.duke.edu']['user'],
            config['https://aka.oit.duke.edu']['key'])

    # Hrm, only the first one seems to matter...🤔
    existing_record = requests.get('https://aka.oit.duke.edu/api/v1/dns/%s' %
                                   target_name,
                                   auth=auth).json()[0]

    edit_path = existing_record['edit_path'].split('?')[0]
    update_record = requests.put('https://aka.oit.duke.edu%s' % edit_path,
                                 auth=auth,
                                 data={
                                     'address': target_ip,
                                 })
    print(update_record)

    return 0


if __name__ == "__main__":
    sys.exit(main())
