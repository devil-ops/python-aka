#!/usr/bin/env python3

import sys
from akapy.helpers import CredentialParser
from akapy.aka import Api


def main():

    cred_parser = CredentialParser("https://aka.oit.duke.edu")
    cred = cred_parser.import_credentials()
    AKA = Api("https://aka.oit.duke.edu", cred)

    res = AKA.query_path('/ip4/subnets')
    for network in res:
        if network['gateway']:
            print(network['gateway'])

    return 0


if __name__ == "__main__":
    sys.exit(main())
