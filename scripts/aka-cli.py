#!/usr/bin/env python3
"""
Command line interface to AKA
"""
import argparse
import sys
import os

import logging
from akapy.helpers import CredentialParser, pp_results, is_ipaddress
from akapy.aka import Api
from ipaddress import ip_address


def parse_arguments():
    """
    Handle argument parsing here
    """
    parser = argparse.ArgumentParser(
        description="Command line application for the AKA application"
    )
    subparsers = parser.add_subparsers(
        dest="action", help="sub-command help", required=True
    )
    parser_query = subparsers.add_parser("query", help="query AKA")
    parser_query.add_argument("target", type=str, help="Entry to query")
    parser_sysconfig = subparsers.add_parser(
        "sysconfig", help="Print out a sysconfig record for a hostname or ip"
    )
    parser_sysconfig.add_argument(
        "target", type=str, help="Entry to show a sysconfig on"
    )
    parser_sysconfig.add_argument(
        "-d", "--device", type=str, default="eth0", help="Device to use in sysconfig"
    )
    parser_update = subparsers.add_parser("update", help="Update a record")
    parser_update.add_argument("target", type=str, help="Target record to update")
    parser_update.add_argument(
        "-t", "--type", type=str, help="Type of record to update", default="A"
    )
    parser_update.add_argument(
        "--ttl", type=str, help="Update the Time to Live for this record"
    )
    subparsers.add_parser("zones", help="Print out a list of zones")
    subparsers.add_parser("subnets", help="Print out a list of subnets")

    setup_a = subparsers.add_parser(
        "setup-a", help="Create an A record.  If it exists, delete and recreate it"
    )
    setup_a.add_argument("dns_name", help="DNS Name for A record", type=str)
    setup_a.add_argument("ip_address", help="IP Address for A record", type=ip_address)

    setup_cname = subparsers.add_parser(
        "setup-cname", help="Create a CNAME.  If it exists, delete and recreate it"
    )
    setup_cname.add_argument("dns_name", help="DNS Name for CNAME record", type=str)
    setup_cname.add_argument("target_name", help="Target for CNAME", type=str)

    setup_host = subparsers.add_parser(
        "setup-host", help="Create an A record and reverse PTR for new host"
    )
    setup_host.add_argument("dns_name", help="DNS Name for host record", type=str)
    setup_host.add_argument("ip_address", help="IP Address for host record", type=str)
    return parser.parse_args()


args = parse_arguments()

if "AKA_BASE_URL" in os.environ:
    AKA_BASE = os.environ["AKA_BASE_URL"]
else:
    AKA_BASE = "https://aka.oit.duke.edu"

cred_parser = CredentialParser(AKA_BASE)
cred = cred_parser.import_credentials()
AKA = Api(AKA_BASE, cred)


def main():
    """
    Main process for application
    """
    if args.action == "query":
        if is_ipaddress(args.target):
            res = AKA.query_path("ip/%s" % args.target, method="get")
            pp_results(res)
            print()
            for title, res in AKA.query_path(
                "ip/%s/dns_records" % args.target, method="get"
            ).iteritems():
                print("DNS Records for %s" % title)
                pp_results(res)
        else:
            res = AKA.query_path("dns/%s" % args.target, method="get")
            pp_results(res)
    elif args.action == "setup-a":
        existing_records = AKA.search_dns(args.dns_name)
        needs_update = False
        has_existing = False
        if len(existing_records) > 0:
            has_existing = True
            for existing_record in existing_records:
                if existing_record["address"].strip() != str(args.ip_address):
                    logging.warning(
                        "Address does not match, deleting existing records in %s"
                        % existing_record["view"]
                    )
                    needs_update = True
        if needs_update and has_existing:
            logging.warning("Deleting existing records for %s" % args.dns_name)
            AKA.delete_dns(args.dns_name)

        if needs_update or not has_existing:
            res = AKA.register_a(args.dns_name, args.ip_address)
            if res:
                print(
                    "Successfully registered %s to %s"
                    % (args.dns_name, args.ip_address)
                )
                return 0
            else:
                print("Failed to register %s" % args.dns_name)
                return 2
    elif args.action == "setup-cname":
        print("Setting up cname")
        existing_records = AKA.search_dns(args.dns_name, record_type="CNAME")
        existing_target_records = AKA.search_dns(args.target_name)
        if len(existing_target_records) == 0:
            logging.error("Could not find any target records for %s" % args.target_name)
            return 5

        needs_update = False
        has_existing = False

        if len(existing_records) > 0:
            has_existing = True
            for existing_record in existing_records:
                if existing_record["rdata"].strip() != str(args.target_name):
                    logging.warning(
                        "Address does not match, deleting existing records in %s"
                        % existing_record["view"]
                    )
                    needs_update = True

        # Delete if we are about to recreate this sucka
        if needs_update and has_existing:
            logging.warning("Deleting existing records for %s" % args.dns_name)
            AKA.delete_dns(args.dns_name, record_type="CNAME")

        if needs_update or not has_existing:
            logging.warning(
                "About to register cname %s to %s" % (args.dns_name, args.target_name)
            )
            res = AKA.register_cname(args.dns_name, args.target_name)
            if res:
                print(
                    "Successfully registered %s to %s"
                    % (args.dns_name, args.target_name)
                )
                return 0
            else:
                print("Failed to register %s" % args.dns_name)
                return 2

    elif args.action == "setup-host":
        existing_records = AKA.search_dns(args.dns_name)
        needs_update = False
        has_existing = False
        if len(existing_records) > 0:
            has_existing = True
            for existing_record in existing_records:
                if existing_record["address"] != args.ip_address:
                    print("Address does not match, deleting existing records")
                    needs_update = True
        if needs_update and has_existing:
            print("Deleting existing records for %s" % args.dns_name)
            AKA.delete_dns(args.dns_name)

        return_code = 0
        if needs_update or not has_existing:
            ptr = AKA.register_ptr(args.ip_address, args.dns_name)
            if ptr:
                print(
                    "Successfully registered PTR %s to %s"
                    % (args.ip_address, args.dns_name)
                )
            else:
                print("Failed to register PTR for %s" % args.dns_name)
                return_code = 2

            res = AKA.register_a(args.dns_name, args.ip_address)
            if res:
                print(
                    "Successfully registered %s to %s"
                    % (args.dns_name, args.ip_address)
                )
            else:
                print("Failed to register %s" % args.dns_name)
                return_code = 2

            return return_code

    elif args.action == "sysconfig":
        from IPy import IP

        if is_ipaddress(args.target):
            ip_res = AKA.query_path("ip/%s" % args.target)
        else:
            dns_res = AKA.query_path("dns/%s" % args.target)
            ip_res = AKA.query_path("ip/%s" % dns_res[0]["address"])

        ip = ip_res["ip"]
        network_obj = IP(ip_res["subnet"])
        network_long_netmask = network_obj.strNormal(2).split("/")[1].strip()
        gateway = AKA.query_path("ip4/subnets/%s" % ip_res["subnet"])["gateway"]

        print("DEVICE=%s" % args.device)
        print("IPADDR=%s" % ip)
        print("GATEWAY=%s" % gateway)
        print("NETMASK=%s" % network_long_netmask)
        print("TYPE=Ethernet")
        print("BOOTPROTO=none")
        print("IPV6INIT=no")
        print("USERCTL=yes")

    elif args.action == "zones":
        res = AKA.query_path("/dns/zones")
        pp_results(res)

    elif args.action == "subnets":
        res = AKA.query_path("/ip4/subnets")
        print("%-20s %-60s %-20s" % ("cidr", "name", "gateway"))
        for item in res:

            # Ug, some netwoks don't have names 😔
            if not item["name"]:
                item_name = "NO_NAME"
            else:
                item_name = item["name"]
                item_name = item_name.replace("\u2013", "-")

            try:
                print(
                    "%-20s %-60s %-20s"
                    % (item["cidr"], item_name.decode("utf-8"), item["gateway"])
                )
            except UnicodeEncodeError:
                # TODO: Handle unicode strings
                logging.error(
                    "%s has unicode characters in it, skipping for now\n" % item
                )
                continue

    elif args.action == "update":
        if args.type == "A":
            records = AKA.query_path("/dns/%s" % args.target)
            print(records)
            for item in records:
                post_items = {
                    "backend_name": item["backend_name"],
                    "type": args.type,
                    "value": item["address"],
                }

                # Update TTL here
                # TODO:  Handle things like 5m, 24h, etc
                if args.ttl:
                    post_items["ttl"] = args.ttl

                res = AKA.query_path(
                    "dns/%s/%s/%s" % (item["backend_name"], args.type, item["address"]),
                    params=post_items,
                    method="put",
                )
                for notice in res["notices"]:
                    print(notice)
                for alert in res["alerts"]:
                    print(alert)
        else:
            sys.stderr.write("This doesn't handle %s records yet\n" % args.type)
            return 255

    return 0


if __name__ == "__main__":
    sys.exit(main())
