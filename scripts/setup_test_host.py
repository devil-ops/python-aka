#!/usr/bin/env python
"""
Setup test data
"""
import os
import logging
from akapy import Colorer
logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.DEBUG)

from akapy.helpers import CredentialParser
from akapy.aka import Api

test_hostname = 'drews-dev-99.oit.duke.edu'
test_cname = 'drews-dev-cname.oit.duke.edu'
test_tmp_name = 'drews-dev-99-aka-mover-temp.oit.duke.edu'
test_network = '10.236.82.0/24'

## Nasty globals
if os.environ.has_key('AKA_BASE_URL'):
    AKA_BASE = os.environ['AKA_BASE_URL']
else:
    AKA_BASE = "https://aka.oit.duke.edu"

cred_parser = CredentialParser(AKA_BASE)
cred = cred_parser.import_credentials()
AKA = Api(AKA_BASE, cred)

existing_ip = AKA.validate_hostname(test_hostname)
if existing_ip:
    AKA.delete_ip(existing_ip)

tmp_ip = AKA.validate_hostname(test_tmp_name)
if tmp_ip:
    AKA.delete_ip(tmp_ip)

new_ip_res = AKA.register_next_available(test_network, test_hostname)

new_cname = AKA.register_cname(test_cname, test_hostname)
