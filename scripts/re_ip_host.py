#!/usr/bin/env python
"""
Script to move a host to a new IP or Network
"""
import os
import sys
## Make the script work without having to install this as a module
sys.path.append("%s/../" % os.path.dirname(__file__))
import argparse
import logging
from akapy import Colorer
from akapy.helpers import CredentialParser, cidr_to_netmask, is_pingable, wait_until_it_pings
from akapy.aka import Api

def parse_args():
    """
    Get the arguments
    """
    parser = argparse.ArgumentParser(description="""

Command line application to re-ip a host.  Use the --network option to move the
host to the next available IP on a network.  Use the --ip option to move it to
a pre-registered IP address

""")
    parser.add_argument('target', metavar='HOSTNAME_TO_MOVE', type=str, help='Hostname to move')
    parser.add_argument('-v', '--verbose', action='store_true', help='Verbose output')
    parser.add_argument('--skip-host-ping', action='store_true',
                        help='Skip the check to see if the host we\'re moving is up')

    ## Move to either a network, or a specific IP
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('-n', '--network', type=str, help="Network to move to")
    group.add_argument('-i', '--ip', action='store_true',
                       help="Move this host to a pre-registered IP Address.")

    return parser.parse_args()


def get_ip_change_command(old_ip, new_ip, new_subnet, new_gateway, hostname, network_label):
    """
    Import how you should do commands from the .akarc file

    [change_ip]
    command = superssh $$OLD_IP$$ /opt/duke/bin/change_ip.py $$NEW_IP$$ $$NEW_SUBNET$$ $$NEW_GATEWAY$$
    """
    command_file = os.path.expanduser("~/.akarc")
    if not os.path.exists(command_file):
        sys.exit("%s is required" % command_file)
    import ConfigParser
    config = ConfigParser.ConfigParser()
    config.read(command_file)

    change_ip_command = config.get('change_ip', 'command')\
        .replace('$$OLD_IP$$', old_ip)\
        .replace('$$NEW_IP$$', new_ip)\
        .replace('$$NEW_SUBNET$$', new_subnet)\
        .replace('$$NEW_GATEWAY$$', new_gateway)\
        .replace('$$HOSTNAME$$', hostname)\
        .replace('$$NETWORK_LABEL$$', network_label)\

    return change_ip_command

## Nasty globals
## Nasty globals
if os.environ.has_key('AKA_BASE_URL'):
    AKA_BASE = os.environ['AKA_BASE_URL']
else:
    AKA_BASE = "https://aka.oit.duke.edu"

cred_parser = CredentialParser(AKA_BASE)
cred = cred_parser.import_credentials()
AKA = Api(AKA_BASE, cred)

def gen_migration_hostname(hostname):
    """
    Given a hostname, generate a migration hostname to use

    Example: drews-dev-01.oit.duke.edu returns drews-dev-01-aka-mover-temp.oit.duke.edu
    """
    append = '-aka-mover-temp'
    (shortname, domain) = hostname.split(".", 1)
    return "%s%s.%s" % (shortname, append, domain)


def main():
    """
    Meaty app goodness
    """
    args = parse_args()
    if args.verbose:
        logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.DEBUG)
    else:
        logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.INFO)



    ## TODO:  Should this really be the way we validate things?
    original_ip = AKA.validate_hostname(args.target)

    ttl = AKA.guess_ttl(args.target)

    if not original_ip:
        logging.error("Error: %s returned too many or too few results", args.target)
        sys.exit(2)

    if not args.skip_host_ping:
        if not is_pingable(args.target):
            logging.error("Error: %s doesn't ping, is that the right name?", args.target)
            sys.exit(2)

    migration_hostname = gen_migration_hostname(args.target)

    if args.network:

        if not AKA.validate_network(args.network):
            logging.error("Error: %s returned too many or too few results", args.network)
            sys.exit(2)

        network_info = AKA.query_path('/ip4/subnets/%s' % args.network)
        target_subnet = cidr_to_netmask(network_info['cidr'])
        target_gateway = network_info['gateway']
        network_label = network_info['name']

        logging.info("We will move %s (%s) now...", args.target, original_ip)
        logging.info("Registering %s on %s", migration_hostname, args.network)
        new_ip_res = AKA.register_next_available(args.network, migration_hostname)

        new_ip = new_ip_res['ip']['ip']
        logging.info("Newly registered IP is %s", new_ip)

    elif args.ip:

        new_ip = AKA.validate_hostname(migration_hostname)
        if not new_ip:
            logging.error("First register '%s' to a valid IP in AKA, then try this script again", migration_hostname)
            sys.exit(5)

        ip_info = AKA.get_ip(new_ip)
        network_info = AKA.query_path('/ip4/subnets/%s' % ip_info['subnet'])
        target_subnet = cidr_to_netmask(network_info['cidr'])
        target_gateway = network_info['gateway']
        network_label = network_info['name']

    ## Placeholder to move host to new IP
    ## TODO:  Automate this
    ip_change_cmd = get_ip_change_command(original_ip, new_ip, target_subnet, target_gateway, args.target, network_label)
    logging.info("We're going to try and run: %s", ip_change_cmd)
    os.system(ip_change_cmd)
    try:
        wait_until_it_pings(new_ip)
    except KeyboardInterrupt:
        logging.info("Moving on...")

    ## TODO:  Iterate through all of the a records

    ## Add new IP to old name A record
    AKA.query_path('dns', method='post', params={
        'name': args.target,
        'type': 'A',
        'ttl': ttl,
        'address': new_ip
    })

    ## Add PTR from new ip to old name
    AKA.query_path('dns', method='post', params={
        'target': args.target,
        'name': new_ip,
        'type': 'PTR',
        'ttl': ttl
    })

    ## Remove tmpname
    AKA.query_path('dns/%s/A/%s' % (migration_hostname, new_ip), method='delete', params={
        'backend_name': migration_hostname,
        'type': 'A',
        'value': new_ip
    })

    ## remove oldname to old ip
    AKA.query_path('dns/%s/A/%s' % (args.target, original_ip), method='delete', params={
        'backend_name': args.target,
        'type': 'A',
        'value': original_ip
    })

    if is_pingable(new_ip):
        logging.info("Successfully moved %s from %s to %s", args.target, original_ip, new_ip)
    else:
        logging.error("Finished moving %s from %s to %s, however the new IP does not yet ping, please investigate", args.target, original_ip, new_ip)

if __name__ == "__main__":
    sys.exit(main())
