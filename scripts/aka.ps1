<#
.Synopsis
   Do some queries against the AKA service
.DESCRIPTION
   Make sure you have a file ~/.akarc that is set up like this:
   [https://aka.oit.duke.edu]
   user = my_user
   key = xxx-yyy

   [https://aka-test.oit.duke.edu]
   user = my_user
   key = aaa-bbb
#>
param (
   [Parameter(mandatory=$true)]
   [string] $dnsname
)

Function Parse-IniFile ($file) {
  $ini = @{}

 # Create a default section if none exist in the file. Like a java prop file.
 $section = "NO_SECTION"
 $ini[$section] = @{}

  switch -regex -file $file {
    "^\[(.+)\]$" {
      $section = $matches[1].Trim()
      $ini[$section] = @{}
    }
    "^\s*([^#].+?)\s*=\s*(.*)" {
      $name,$value = $matches[1..2]
      # skip comments that start with semicolon:
      if (!($name.StartsWith(";"))) {
        $ini[$section][$name] = $value.Trim()
      }
    }
  }
  $ini
}

$aka_base = 'https://aka.oit.duke.edu'
$auth = Parse-IniFile('~/.akarc')
$username = $auth.$aka_base.user
$password = $auth.$aka_base.key
$aka_url = "${aka_base}/api/v1/dns/$dnsname"
Write-Host "URL: $aka_url"

$base64AuthInfo = [Convert]::ToBase64String([Text.Encoding]::ASCII.GetBytes(("{0}:{1}" -f $username,$password)))

$response = Invoke-RestMethod -Method Get -Uri $aka_url -Headers @{Authorization=("Basic {0}" -f $base64AuthInfo)}

$address = $response | Select-Object address

Write-Host "$dnsname has an address of $($address.address)"
